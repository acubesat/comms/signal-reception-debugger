# Signal Reception Debugger

A GRC-based application for debugging the effective radiated power (ERP) of the on-board transmitters

![GRC Flowgraph](https://i.imgur.com/QsSuufZ.png "GRC Flowgraph")

## GRC Flowgraph Remarks

#### (1) - Center frequency
Change to S-band if necessary (depending on the antenna under test). If DC spike is present, you can shift the center frequency by a few kHz to avoid confusion ("interference").

#### (2) - Sample rate/bandwidth
Change depending on PlutoSDR's bandpass shape (I'd be careful to choose an instantaneous bandwidth greater than the transmission bandwidth, but narrow enough to avoid filter roll-off, although that might not be very important for this application).

#### (3) - Number of channels (FFT size)
8192 is probably sufficient, although I'm not sure what FFT size is used in the demodulation/decoding flowgraphs. Feel free to increase as long as it does not present a significant computational expense for the computer the flowgraph is running on (e.g. Raspberry Pi).

#### (4) - Number of bins
This determines the integration time per FFT (spectrum) sample (`T_int` or `T_sample`). The integration time per FFT sample is defined as the ratio of the product of the number of bins, multiplied by the number of channels to the sample rate (`n_bins` x `n_chan`/`samp_rate`, `n_bins` ∈ ℕ*, `n_chan` ∈ 2^n, n∈ℕ). Generally, the number of bins should not significantly affect the averaged spectrum. As `n_bins` → 1, the timing resolution increases, while the total data saved to file also increases proportionally.

#### (5) - Observation time
Here, you are free to change the total duration of the observation. The signal-to-noise ratio should increase by the square root of the duration of the observation (S/N ~ √`obs_time`)\*.

\* Assuming the system temperature (noise floor) is constant, the effective radiated power (ERP) of the on-board transmitter is fixed and the observing bandwidth is greater or equal to the transmission bandwidth (always the case for us of course).

#### (6) - Source block
Replace the `Null Source` block with a `PlutoSDR Source` block and set the corresponding `Device URI` parameter if necessary.

#### (7) - Polyphase filter
In traditional introductory flowgraphs, this can generally be bypassed (ignored) due to its (seeming) complexity. When it comes to spectral leakage however, the implementation of a polyphase filterbank has a significant advantage to Fourier transform filterbanks (FTF) and autocorrelation spectrometers (ACS), as seen in [Fig. 3](https://arxiv.org/pdf/1607.03579.pdf).

#### (8) - Spectrometer
This part of the flowgraph computes the FFT and integrates (averages) the data (FFT spectra) together, based on the `Decimation` factor which is set to the number of bins (`nbin`) described above.

#### (9) - File Sink
The output of the collected data, stored (appended) to the given `.dat` file for further processing by the Python plotter script. Make sure the path of the file is set!

#### (10) - Head block (forgot to remark)
The head block, along with the `Run to Completion` run option (at the top left corner of the flowgraph) is set so the GRC flowgraph ends at the given duration, which is passed as an argument by the user in the Python plotter script.